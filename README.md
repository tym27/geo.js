# geo.js

A small Nodejs script that returns latitude, longitude, and [what3words](https://what3words.com/) coordinates to the console. [Note: uses `macos-location` library, so obviously Mac-only. I was looking for a way to get CoreLocation output on my laptop, and this is easiest.]

## Install

```
git clone
yarn install
ln -s /path/to/geo.sh /usr/local/bin/geo
```

## Use

For example:

```
~> geo
lat: 41.87902832
long: -87.63579517
w3w: rigid.tones.swaps
```
