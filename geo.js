#!/usr/bin/env node

const { getCurrentPosition } = require('macos-location');
const OpenLocationCode = require("node-pluscodes/openlocationcode")

function successCallback(position) {
  var lat = position.coords.latitude.toFixed(8) 
  var long = position.coords.longitude.toFixed(8)
  console.log(`lat: ${ lat }`);
  console.log(`lng: ${ long }`);
  var pluscode = OpenLocationCode.encode(lat, long, 11);
  console.log(`olc: ${ pluscode }`);
};

function errorCallback(err) {
  console.warn(`ERROR(${err.code}): ${err.message}`);
};
 
// https://developer.mozilla.org/en-US/docs/Web/API/PositionOptions
const options = {
  maximumAge: 1000,
  enableHighAccuracy: true,
};
 
getCurrentPosition(successCallback, errorCallback, options);
